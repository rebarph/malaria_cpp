#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
using namespace cv;
using namespace std;
int main( int argc, char** argv )
{
    // We need an input image. (can be grayscale or color)
    if (argc < 2)
    {
        cerr << "We need an image to process here. Please run: colorMap [path_to_image]" << endl;
        return -1;
    }
    Mat img_in = imread( argv[1], 1 );

    if(img_in.empty())
    {
        cerr << "Sample image (" << argv[1] << ") is empty. Please adjust your path, so it points to a valid input image!" << endl;
        return -1;
    }

    // Thresholding using THRESH_TRUNC
    Mat threshold_img;
  	threshold( img_in, threshold_img, 127, 255, THRESH_TRUNC);
    imshow( "Threshold", threshold_img );

    //Perform edge detection then perform a dilation + erosion to
    //close gaps in between object edges
    Mat edge_detect_img;
    Mat edge;
    //Correct scale
    Canny( threshold_img, edge, 50, 200, 3);
    edge.convertTo(edge_detect_img, CV_8U);
    imshow( "Edge Detection", edge_detect_img );
    //Dilate
    Mat dilate_img;
    dilate( edge_detect_img, dilate_img, Mat());
    imshow( "Dilate", dilate_img );
    //Erode
    Mat erode_img;
    erode( dilate_img, erode_img, Mat());
    imshow( "Erode", erode_img );

    // Holds the colormap version of the image:
    Mat colormap_img;
    // Apply the colormap:
    //applyColorMap(erode_img, colormap_img, COLORMAP_JET);
    applyColorMap( threshold_img, colormap_img, COLORMAP_HSV );
    //applyColorMap(threshold_img, colormap_img, COLORMAP_HOT);
    // Show the result:
    imshow("Color Map", colormap_img);

    //Grayscale;
    Mat grayscale_img;
    cvtColor( colormap_img, grayscale_img, COLOR_BGR2GRAY );
    imshow( "Grayscale", grayscale_img );

    //Gaussian filtering
    Mat gaussian_img;
    int i = 7;
    GaussianBlur( grayscale_img, gaussian_img, Size(i,i), 0, 0 );
    const char* source_window = "Gaussian Blur";
    namedWindow( source_window, WINDOW_AUTOSIZE );
    imshow( source_window, gaussian_img );

    //Find contours in the edge Map
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    findContours( erode_img, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

    /// Find the rotated rectangles and ellipses for each contour
    vector<RotatedRect> minRect( contours.size() );
    vector<RotatedRect> minEllipse( contours.size() );
    int points = 50;
    RNG rng(12345);

    for( int i = 0; i < contours.size(); i++ )
    {
       minRect[i] = minAreaRect( Mat(contours[i]) );
       cout<<endl<<minRect[i].size<<endl;
       if( contours[i].size() > points )
         { minEllipse[i] = fitEllipse( Mat(contours[i]) ); }
    }

    /// Draw contours + rotated rects + ellipses
    Mat drawing = Mat::zeros( erode_img.size(), CV_8UC3 );
    for( int i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 255), rng.uniform(0,255), rng.uniform(0,255) );
        // contour
        drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
        // ellipse
        ellipse( drawing, minEllipse[i], color, 2, 8 );
        // rotated rectangle
        Point2f rect_points[4]; minRect[i].points( rect_points );
        for( int j = 0; j < 4; j++ )
           line( drawing, rect_points[j], rect_points[(j+1)%4], color, 1, 8 );
    }

    /// Show in a window
    namedWindow( "Contours", WINDOW_AUTOSIZE );
    imshow( "Contours", drawing );
    //Handle dusty noise
    //medianBlur(bgr_image, bgr_image, 3);

    //Convert BGR to HSV
    //Mat hsv_img;
    //cvtColor(colormap_img, hsv_img, COLOR_BGR2HSV); //to convert RGB to HSV
    //imshow("HSV Color Map", hsv_img);

    /*// Find dark images
    Mat lower_hue_range;
    Mat upper_hue_range;
    Mat hue_img;
    //inRange(colormap_img, Scalar(0, 100, 100), Scalar(0, 255, 255), lower_hue_range);
    //inRange(colormap_img, Scalar(160, 100, 100), Scalar(179, 255, 255), upper_hue_range);
    inRange(colormap_img, Scalar(0, 0, 255), Scalar(0, 255, 255), hue_img);
    //addWeighted(lower_hue_range, 1.0, upper_hue_range, 1.0, 0.0, hue_img);
    GaussianBlur(hue_img, hue_img, Size(9, 9), 2, 2);
    imshow( "Target Objects", hue_img );
    */


    //Review erosion and dilation, might need structuring element
    //dilate( edge_detect_img, edge_detect_img, Mat());

    Mat dftInput1, dftImage1, inverseDFT, inverseDFTconverted;
    erode_img.convertTo(dftInput1, CV_32F);
    dft(dftInput1, dftImage1, DFT_COMPLEX_OUTPUT);    // Applying DFT

    // Reconstructing original image from the DFT coefficients
    idft(dftImage1, inverseDFT, DFT_SCALE | DFT_REAL_OUTPUT ); // Applying IDFT
    inverseDFT.convertTo(inverseDFTconverted, CV_8U);
    imshow("Inverse DFT", inverseDFTconverted);

    waitKey(0);
    return(0);
}
